+++ 
draft = false
date = 2020-05-21T08:32:49-05:00
title = "Dusting Off the Cobwebs"
description = "Digging through my commit history to remember how I built this site almost a year ago"
slug = "" 
tags = ["Hugo", "Netlify", "Cloudflare", "GitLab"]
categories = []
externalLink = ""
series = []
+++

#### Then

So I just built this website around August of last year -- mainly just a landing page for my repositories and my social media. 
I had just learned about static site generators (SSGs) and became really enthralled with the idea.

My job involved working on web applications with large and complicated back ends, so there was just something about the idea of serving up simple HTML and CSS that was really refreshing to me. 
Plus the fact that they were easily styled with themes and made use of templating to reuse components and easily add content. I was all on board.


After looking into the different SSGs, I settled on [Hugo](https://gohugo.io/) for it's fast compile times and for the fact that it would allow me to work with Go again, as I had been working with Node for several months. 

#### Now

So a couple of weeks ago, I get the itch to update my site a little bit, since it's about as bare bones as you can get. There was only one small issue: _I didn't exactly remember how I set everything up._

That's cool though. It happens... Right?

So here I am. Dusting off the cobwebs, and playing detective on an almost-year-younger version of myself. 

#### What I Did

So. Here's what I found out:

I remember doing a bunch of research on just website hosting in general. At the time, I embarrassingly didn't know everything that went into it: the domain name registration, where and how to host, how much it cost, how to deploy it, etc. 

So I had purchased my domain through 1&1. I don't exactly remember the reason... I think the free WhoIs protection was a factor.

Then I found out that you could host static sites for free on GitHub or GitLab. I decided on GitLab because of their built in CI. I realize you don't need that extravagant of a pipeline for a SSG, but there's just something about those little green check marks on pushed commits that make you feel all warm and fuzzy inside. 

I remember having to futz with the `.gitlab-ci.yml` a bit in order to get things working. But after that, it was pretty easy to point my domain to the deployed site.

Then I remember discovering [Netlify](https://www.netlify.com/) and I was excited to play around with the new and shiny thing. So I kept my repo on GitLab, but shifted the deployment responsibilities and hosting to Netlify.

The pipeline was pretty easy to setup if I remember correctly. But I wanted to try out the Deploy Previews feature. With a little bit of configuring, you can setup a hook where each time you open a MR against your master branch on GitLab, Netlify builds and deploys that version of your site to a temporary URL.
The best part  is that it posts the status of this operation into a comment on the actual MR and also provides you the link to the deployed version after it completes the build. Neat right?!

If you need to push another commit to the MR, Netlify rebuilds and redeploys your site again with the new changes, editing the comment to display the status of the operation again and letting you know once it's done.

As soon as you merge the MR into your master branch, Netlify builds and deploys to your live site. Super easy.

I do recall hitting a bit of the snag with the CSS not rendering right on the Deploy Preview due to a CORS error.
The issue was that since the URL of the Deploy Preview site was not my domain name and therefore the URL of the deployed site, it wouldn't allow access to the CSS, since the URLs didn't match.

Turns out Netlify gives you a few environment variables to use and I was able to use one in my `netlify.toml` so that way the CSS could be found at the same URL as the rest of the site.

Oh. And at some point in my journey, I learned about [Cloudflare](https://www.cloudflare.com/) and how useful caching was especially in static sites, so I migrated from using my domain registrar's name servers to using Cloudflare's.

#### Updates and Future Plans

What a fun little trip down memory lane. 

So, some of the updates I've made so far have been pretty small.

I whipped up a Favicon in GIMP real quick to use here.
I'm actually pretty proud of it. My graphic design skills leave a lot to be desired, but once I was done I was actually surprised with how incredibly decent it looks.
I also updated the Avatar on my home page to a more current photo.

Lastly, I'm adding a blog (as you can now see), which I plan on using to document any side projects I work on. 

Mainly so that I don't have to throw on a detective hat anymore to remember out how I built them.
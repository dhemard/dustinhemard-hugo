+++ 
draft = false
date = 2020-10-13T11:30:00-05:00
title = "Revisiting eReaders Almost Ten Years Later"
description = "My experience researching, selecting, and configuring a new eReader while overcoming a few technical hurdles along the way."
slug = "" 
tags = ["Kobo", "Calibre", "eBook"]
categories = []
externalLink = ""
series = []
+++

# My first eReader

Back in 2011, Barnes & Noble released their new version of the Nook, the *Nook Simple Touch*. 
At the time I was reading pretty voraciously, taking trips to the bookstore weekly, and filling up my bookshelf at a pretty steady rate.
I had seen the Nooks on display in store, so I decided that I'd try out reading on an eInk display.

I love the tangible act of flipping through pages of books.
The physical process of going to the bookstore to get one: walking through the shelves, picking up something with an interesting title or cover, reading the summary on the back or turning to a random page in the middle to see what it's like, adding it to your stack of prospective choices in your arms, wandering to another section of the store to try out a different genre, eventually whittling that stack down and finally making a selection (or two), excitedly bringing the book home, eagerly finding your quiet spot to begin, opening the cover and smoothing it open to the first page, lifting the page and placing your index finger below it as you near the bottom so you waste no time flipping to the next.

Even still, I gave the Nook a chance. Two of the first few books I purchased on the Nook were *The Hobbit* and *The Lord of the Rings* -- collectively over 1500 pages.
It was so astonishing to me that I could carry around such massive and epic texts in such a small package.
That was my first readthrough of the series, and I cherish the experience to this day.

But it wasn't just the fact that I could hold an entire bookshelf in my hands that had me excited about it.
When I finished a book, now I could just download another one as long as I had an internet connection.
Since I would read at night usually, this meant no late-night trip to the bookstore needed.
Speaking of reading at night, with the attachable reading light I could do so easily without the lights on in my room.
I also loved how long the battery would last - around a month if I remember correctly.

I was sold on eReaders at that point.
For me, they could never replace physical books, but they were a fantastic companion to them all the same.

Over the next couple years, though, I began to read less and less.
Responsibilities and life began to make my days feel so short, and social media and the distraction of my smartphone took up more and more of my laying-in-bed-before-sleep time.

My bookshelf got dusty.
My Nook sat uncharged.

# Curiosity piqued again

As I was moving at the beginning of this year, I rediscovered my old Nook.
The screen was thoroughly scratched through my neglectful treatment of it over the years.
It had been left in the back of a car in the Louisiana summer heat for weeks at one point.
The rubber around the edges of the screen had become sticky and soft.
But I charged it, and it still worked!
All of the books I had collected on it were still there!
The only thing was that using it had now become a physically unpleasant reading experience with how beaten up it was.
I made a mental note that I should look into replacements and try to get back into reading.


But life got in the way again, and I never got around to it.

A few months later I was hanging out with a friend who had a *Kindle Paperwhite*.
She absolutely loved it and showed it off to me and let me use it for a bit.
The glass screen was front-lit and the lettering was crisp.
The touchscreen was responsive and quick.
She had a cool case for it that would put it to sleep when you closed the flap like a book cover.
It analyzed your reading speed and told you how much longer it would take you to finish the chapter.
I was thoroughly impressed with how far eReaders had come since I had last bought mine.
I decided to get a replacement for my old Nook and began researching what options were out there.

# Kindles

I started my search with the Kindle Paperwhite.
Kindles are pretty synonymous with eReaders nowadays, and I did like a lot of things about my friend's Kindle: the fact that it was waterproof, the glass screen, the frontlight.
I didn't, however, like the fact that I needed to pay $20 more just to get a version that didn't show me ads.

I also didn't like the fact that being tied to the Kindle/Amazon ecosystem for your reading material was pretty much mandatory.
As I had mentioned before, I already had quite a few purchases from the Barnes & Noble eBook store as well as some other eBooks I obtained on the Google Play Books store.
The EPUB format for eBooks is not supported on the Kindle.
And the vast majority of the eBooks that I already owned were in that format.

It was mainly this fact that led me to search for alternative eReaders.

# Kobos

A quick googling of "Kindle alternatives" led me to Kobo.
They're a Canadian company owned by Rakuten that produce a number of different eReaders.
I had my eye on the *Kobo Clara HD*, which, out of their products, seemed most similar in size and features to the Kindle Paperwhite.

After a bit of research, some things about the Clara HD that I wasn't crazy about were the fact that it came from a less known company (in the US, at least. I've since learned that Rakuten is basically the Japanese Amazon) and that its screen was recessed and plastic.
I hadn't personally heard of Kobo before I started this eReader search, so I was unsure what their reputation was like.
Customer service, product reliability, consistent software updates, and a great user experience are all things that are important to me with a purchase like this.
So not knowing how Kobo faired in these categories went into my list of cons.
The recessed screen was another thing that I was concerned about.
My Nook had a recessed plastic screen so far my experience with it was that and it collected a lot of dust and debris in the recess and the screen had become very scratched.
The Kingle Paperwhite's screen definitely had the edge here.

What I did like about the Kobo eReader was that it supported both EPUBs and PDFs (in addition to it's own eBook format known as KEPUB), its frontlight had blue light reduction, it had OverDrive support integrated, and it was just generally more "hackable".
The fact that I could transfer my existing eBook library onto it was the main selling point for me.
I really like the fact that it was way less locked down that the Kindle was.
OverDrive--an application that allows you to borrow eBooks easily from a public library--was something that I actually hadn't heard of before I started researching, but I was pleasantly surprised once I learned about how much more seamlessly it was integrated into the Kobo that with the Kindle.
The front light, called Comfortlight, has a mix of both white and orange LEDs which allows for a gradual shift to a warmer light for night time reading without disrupting sleep cycles.
The Kindle Paperwhite only had white LEDs in it's front light.
Finally, while the Kobo eReader does come preinstalled with a variety of different fonts, I learned that it was even possible to sideload additional fonts onto the reader--another point for hackability!

# The wait

So I ordered the Kobo Clara HD and a case for it as well.
While I waited for it to arrive, I read up on things to do with it and ways to customize it.

I downloaded a few different fonts and researched how to transfer them to my Kobo.
As I mentioned before, it comes with several fonts to choose from.
In my research of the Kindle I had seen how well received Amazon's Bookerly font was, however.
Fortunately for me I was able to find the ttf files, which meant that I could try it out on my Kobo.
Also, to be quite honest, the process of sideloading fonts just seemed kind of... _neat_.
It very much appealed to my "tinkery" side.

Next I applied for a library card at my local library and I signed up for an OverDrive account as well, so that I'd be able to borrow books onto my Kobo.

I also found a website that had a collection of literature whose copyrights had expired and thus allowed them to be distributed freely.
It's called [Standard Ebooks](https://standardebooks.org/), and it hosts a collection of free and open eBooks that have had their formatting improved and their text proofread.
I downloaded a few classics and added them to my Calibre library in preparation to transfer them to my Kobo.

Oh and if you hadn't already heard of it, [Calibre](https://calibre-ebook.com/) is an amazing application for managing your electronic libraries.
With it you can transfer eBooks to and from your eReaders (_many_ different eReaders are supported), convert them into different formats, discover and download missing metadata or eBook covers, and lots more!

# Time to set it up

Finally my Kobo arrives and I charge it fully, and then I get to tinkering!

## Borrowing an eBook

I had recently joined a book club with some coworkers.
So this was a great opportunity to test out borrowing an eBook.
The process was pretty painless, as I had already applied for a library card and set up an OverDrive account.
I just had to do the following:

- Link my OverDrive account to my Kobo by going to **Settings -> OverDrive** and logging in with my account
- Go to the **Discover** tab and search for the book I wanted to borrow (I had already confirmed it was available to borrow at my local library)
- Click the three dots on the book to bring up the menu
- Select "Borrow with OverDrive"

My book then downloaded and appeared in my list of Books.
Because I initiated the loan on the Kobo it automatically downloads in the KEPUB format, so features like the time estimate to finish parts of the book work correctly.

## Transferring my free eBooks

After this, I wanted to move the eBooks that I had downloaded from Standard Ebooks onto my Kobo.
I plugged in my Kobo, and loaded up Calibre, where it showed up as a device.
I selected the books I wanted to transfer, right click on them, and select "Send to device".

Seemed simple enough.

There was a small hiccup though.
On my Kobo the books were showing that they were still in EPUB format.
This meant that some of the eBook features present with KEBPUB books weren't working, one of the biggest being the reading analytics and the time estimates.

After searching around I discovered that there was a plugin for Calibre called **KoboTouchExtended** that, once installed, converts EPUBs in your Calibre library to KEPUB format as you transfer them to your Kobo.

Installing the plugin was pretty straightforward:

- In Calibre, go to **Preferences**
- Under "Advanced", select **Plugins**
- Use the search bar to find the plugin
- And finally, with the plugin selected, toggle **Enable/disable plugin**

My EPUBs were then converted in transit to my Kobo while remaining in EPUB format in my Calibre library. 

## Sideloading Fonts

In order to sideload fonts to your Kobo, it's really very simple:

- Connect your Kobo to your computer via USB
- View your Kobo in your computer's file explorer and add a folder called `fonts/` in the root directory of the device
- Copy all the font files into that new folder

And that's it. Man, everything has been pretty easy so far! I'm sure everything else will be just as simple!

_**cue ominous tones**_

# _DRM-mageddon_

My last setup task was to find a way to transfer the eBooks from my old Nook to my Kobo so that I'd be able to actually enjoy reading them on the new device.

I knew that I needed access to the EPUB files of these books.
So I plugged in my Nook expecting to see the EPUB files right there in the file explorer.
I guess I shouldn't have been surprised to see that they were not as easily accessible as I thought and were not there.
Perhaps Calibre would be able to see them?
Unfortunately, that was not the case either, and the eBooks on my Nook that I purchased from the device were hidden from Calibre as well.

After some research I learned that if I downloaded the Nook Windows 10 app, I could log into it with the same account that I was logged into my Nook with, and that I would be able to simply download books in EPUB format from the app.
Cool.
Easy enough.

I download the EPUB files using the Nook app.
I import the EPUBs into Calibre.
I transfer them onto my Kobo.
Everything looks good!
I select one of the these on my Kobo to open it and...

_I see an error stating that my device is not authorized to view the eBook._

DRM, or Digital Rights Management, is a slightly divisive topic.
It was created in order to control the use of digital media which is much more easily copied and distributed than physical media.

I won't be getting into the different arguments for or against DRM.
In this particular situation, I had purchased some digital media--eBooks in this case--from the Barnes & Noble store on my Nook.
All I wanted was the ability to view the media on a different device.
Not even at the same time!
So it wasn't like I wanted two separate copies of the same media.
I was not intending to make duplicates of my eBooks and distribute them.
I just wanted the ability to consume the media that I had purchased.

It is at this point that I began looking for solutions to my conundrum. 

## Bruce Willis

After a cursory googling, I found that there exists an add-on extension for Calibre that removes DRM from eBooks.
I downloaded and installed the extension and tried to transfer over the files again.
Unfortunately, this did not actually remove the DRM, and I was still unable to view the books.
I dejectedly returned to Google.

After hours and hours of digging through old forum posts, GitHub issues, Calibre documentation, and at some points going well past the **sixth** and even the **_seventh_** page of Google search results (usually a good sign your search will be futile), I stumbled upon [this well written and easy to follow blog post](https://www.aricrenzo.com/2019-12-13-Liberate-Your-Nook-Ebooks/) that provided a solution to my exact problem.

The details are spelled out and well documented on that post, but I'll summarize the process here just because of its novelty.
I'll admit that there were a few times where I thought I would never see straight again from how hard I rolled my eyes that _this_ was the process that was necessary just for me to read some of my eBooks on another device.

- First you have to download and install Android Studio and create an AVD of a Nexus 5 phone running Android 7.1   **_...cue the initial eyeroll_**
- Then you have to sideload the Nook Android app from 4 or 5 versions ago onto the AVD
- Next you log in with your B&N account and download the EPUBs onto the Virtual Device
- Then you have to root the AVD and search through its filesystem for the EPUB files _as well as the app's SQLite database_
- Then you query for and export the hash of the key that will decrypt the EPUBs from a table in the downloaded SQLite database and save and import that key into the Calibre extension that removes DRM   **_...second, more painful eyeroll_**
- Finally you can import and transfer those now-DRM-free eBooks onto your eReader

After incredulously completing all the steps, I could now open my old eBooks on my Kobo without issue.

# My overall experience with Kobo

Obviously the process of setting up and configuring a Kobo doesn't need to be as convoluted as my admittedly self-imposed adventure was.
Most people will likely just set up their account, purchase an eBook, and begin reading.

After using my eReader a bit and finishing a couple of books, my overall experience has been positive, and I'm very satisfied with my choice of purchasing a Kobo instead of a Kindle.

My favorite things about my Kobo are it's regular firmware updates, the long-long battery life, the auto blue light filter, the informative reading analytics, and the Kobo store and OverDrive integration.

Some things that I don't like about it are that the screen and outer frame of the reader are kind of fingerprint magnets, and at one point several weeks after configuring it, it got stuck in a boot loop that I needed to resort to a factory restore to solve.

The fingerprint issue may be due to the fact that I have pretty oily skin, however.
And as for the boot loop, I have a hunch that perhaps a combination of a firmware update and my sideloaded fonts configuration may have contributed to it.
After the factory restore I did not sideload the fonts again, and I have not encountered any other boot loop issues after several more firmware updates.

# Turning the page

Although I hope that some of my opinions and issues in this post will help someone else in trying to decide on what eReader to get or trying to regain access to some of their old eBooks, mostly it was just fun to write and retell this journey I went through.

I'm glad to say that getting my Kobo has gotten me back into reading again and made me excited to pick up a book in the evening instead of my phone.

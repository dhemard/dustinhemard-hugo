---
title: "About"
date: 2019-08-01T21:41:48-05:00
draft: false
---

I'm a backend focused software engineer currently living and working in New Orleans, LA.


Outside of work I enjoy cooking, running, traveling, and building mechanical keyboards.

This is my personal website where you can find links to my repositories and social media, as well some of my ramblings on the things that interest me. Thanks for stopping by!

[![Netlify Status](https://api.netlify.com/api/v1/badges/a5503456-2695-4b7a-bd96-467de06c20b6/deploy-status)](https://app.netlify.com/sites/dustinhemard/deploys)

My Personal Website

- Built with [Hugo](https://gohugo.io/) and deployed with [Netlify](https://www.netlify.com/)
